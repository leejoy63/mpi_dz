program main
use :: Task
use :: mpi
implicit none

real(8), dimension(1015,1015) :: A 
integer(4) :: x1, y1, x2, y2, i, j
real(8) :: start_time, finish_time
integer(4) :: mpiErr




do i = 1, 1015
	do j = 1, 1015
		A(i, j)=i+j+1000000
	end do
end do
    call mpi_init(mpiErr)
	start_time = MPI_Wtime()

    call  GetMaxCoordinates(A, x1, y1, x2, y2)

	finish_time = MPI_Wtime()
    call mpi_finalize(mpiErr)

write(*,*) x1,', ', y1,', ', x2,', ', y2

print *, 'Время вычислений = ', (finish_time - start_time)

end
